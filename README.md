# Dododon

A compiler for a new programming language with a Bulgarian-based syntax

## Dependencies

- npm
- javac
- java

## Build

```bash
npm run odc-build
```

## Run an example

```bash
npm run main-build ./odc/examples/src/здравей_свят/ ./odc/examples/build/здравей_свят
npm run main-run ./odc/examples/build/здравей_свят
```
